from flask import Flask, render_template, request, redirect, url_for
import pymysql

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/consultar')
def consultar():
    try:
        conn = pymysql.connect("localhost", "master", "m4st3r123", "materias")
        cursor = conn.cursor()
        cursor.execute("SELECT nombre FROM profesor")
        filas = cursor.fetchall()
        return render_template("consultar.html", filas=filas)
    except:
        print("Error con la base de datos")
    finally:
        cursor.close()
        conn.close()


@app.route('/crear', methods=['GET','POST'])
def crear():
    if request.method == 'GET':
        return render_template("crear.html")
    if request.method == 'POST':
        nombre = request.form['nombre']
        cedula = request.form['cedula']
        conn = pymysql.connect("localhost", "master", "m4st3r123", "materias")
        try:
            cursor = conn.cursor()
            cursor.execute('INSERT INTO profesor VALUES ("{}", "{}")'.format(cedula, nombre))
            conn.commit()
        except:
            print("Error con la base de datos")
        finally:
            cursor.close()
            conn.close()
        return redirect(url_for('consultar'))


@app.route('/modificar', methods=['GET','POST'])
def modificar():
    if request.method == 'GET':
        try:
            conn = pymysql.connect("localhost", "master", "m4st3r123", "materias")
            cursor = conn.cursor()
            cursor.execute("SELECT cedula FROM profesor")
            filas = cursor.fetchall()
            return render_template("modificar.html", filas=filas)
        except:
            print("Error en la base de datos")
        finally:
            cursor.close()
            conn.close()
    if request.method == 'POST':
        nombre = request.form['nombre']
        cedula = request.form['cedula']
        conn = pymysql.connect("localhost", "master", "m4st3r123", "materias")
        try:
            cursor = conn.cursor()
            cursor.execute('UPDATE profesor SET nombre = "{}" WHERE cedula = "{}"'.format(nombre, cedula))
            conn.commit()
        except:
            print("Error en la base de datos")
        finally:
            cursor.close()
            conn.close()
        return redirect(url_for('consultar'))


@app.route('/eliminar', methods=['GET','POST'])
def eliminar():
    if request.method == 'GET':
        try:
            conn = pymysql.connect("localhost", "master", "m4st3r123", "materias")
            cursor = conn.cursor()
            cursor.execute("SELECT cedula FROM profesor")
            filas = cursor.fetchall()
            return render_template("eliminar.html", filas=filas)
        except:
            print("Error en la base de datos")
        finally:
            cursor.close()
            conn.close()
    if request.method == 'POST':
        cedula = request.form['cedula']
        conn = pymysql.connect("localhost", "master", "m4st3r123", "materias")
        try:
            cursor = conn.cursor()
            cursor.execute('DELETE FROM profesor WHERE cedula = "{}"'.format(cedula))
            conn.commit()
        except:
            print("Error en la base de datos")
        finally:
            cursor.close()
            conn.close()
        return redirect(url_for('consultar'))


if __name__ == "__main__":
    app.run(debug=True)
